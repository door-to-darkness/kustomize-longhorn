# Kustomize - Longhorn
[Longhorn](https://github.com/longhorn/longhorn) is a distributed block storage system for Kubernetes. Longhorn is cloud native storage because it is built using Kubernetes and container primitives.
